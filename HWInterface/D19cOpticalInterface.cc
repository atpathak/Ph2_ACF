#include "D19cOpticalInterface.h"

using namespace Ph2_HwDescription;

namespace Ph2_HwInterface
{
// ##########################################
// # Constructors #
// #########################################

D19cOpticalInterface::D19cOpticalInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable) : FEConfigurationInterface(pId, pUri, pAddressTable)
{
    fType = ConfigurationType::IC;
}

D19cOpticalInterface::D19cOpticalInterface(const std::string& puHalConfigFileName, uint32_t pBoardId) : FEConfigurationInterface(puHalConfigFileName, pBoardId) { fType = ConfigurationType::IC; }

D19cOpticalInterface::~D19cOpticalInterface() {}
// ##########################################
// # Chip Register read/write #
// #########################################

bool D19cOpticalInterface::SingleRead(Chip* pChip, ChipRegItem& pItem)
{
    std::lock_guard<std::recursive_mutex> theGuard(fMutex);
    flpGBTSlowControlWorkerInterface->SelectLink(pChip->getOpticalGroupId());
    uint8_t cFunctionId = (pChip->getFrontEndType() == FrontEndType::LpGBT) ? LpGBTSlowControlWorker::SINGLE_READ_IC : LpGBTSlowControlWorker::SINGLE_READ_FE;
    auto    cCommand    = flpGBTSlowControlWorkerInterface->EncodeCommand(cFunctionId, pChip, pItem);
    flpGBTSlowControlWorkerInterface->WriteCommand(cCommand);
    int cWaitCounter = 1000;
    while(!flpGBTSlowControlWorkerInterface->IsDone(cFunctionId) && (cWaitCounter != 0))
    {
        cWaitCounter--;
        continue;
    }
    if(cWaitCounter == 0)
    {
        uint16_t    cState           = flpGBTSlowControlWorkerInterface->GetStateFSM(cFunctionId);
        uint8_t     cWorkerState     = (cState & 0xFF);
        uint8_t     cFunctionState   = (cState & (0xFF << 8)) >> 8;
        std::string cWorkerStateDesc = LpGBTSlowControlWorker::WORKER_FSM_STATE_MAP.at(cWorkerState);
        std::string cFunctionStateDesc =
            (pChip->getFrontEndType() == FrontEndType::LpGBT) ? LpGBTSlowControlWorker::IC_FSM_STATE_MAP.at(cFunctionState) : LpGBTSlowControlWorker::FE_FSM_STATE_MAP.at(cFunctionState);
        LOG(ERROR) << BOLDRED << "D19cOpticalInterface::SingleWrite : Tool stuck - Worker state = " << cWorkerStateDesc << " - Function State = " << cFunctionStateDesc << RESET;
        return false;
    }
    uint8_t cTryCntr = flpGBTSlowControlWorkerInterface->GetTryCntr(cFunctionId);
    if(cTryCntr > 0)
    {
        uint8_t cMaxRetry = (pChip->getFrontEndType() == FrontEndType::LpGBT) ? fConfiguration.fMaxRetryIC : fConfiguration.fMaxRetryFE;
        LOG(ERROR) << BOLDRED << "D19cOpticalInterface::SingleRead : Tried " << +cTryCntr << "/" << +cMaxRetry << " before success" << RESET;
    }
    auto    cReply     = flpGBTSlowControlWorkerInterface->ReadReply(1);
    uint8_t cErrorCode = (cReply[0] & (0xFF << 8)) >> 8;
    if(cErrorCode != 0)
    {
        LOG(ERROR) << BOLDRED << "D19cOpticalInterface::SingleRead : Error Code is " << +cErrorCode << RESET;
        return false;
    }
    pItem.fValue = (cReply[0] & (0xFF << 0)) >> 0;
    return true;
}

bool D19cOpticalInterface::WriteChipRegister(Chip* pChip, ChipRegItem& pItem, bool pVerify)
{
    std::lock_guard<std::recursive_mutex> theGuard(fMutex);
    flpGBTSlowControlWorkerInterface->SelectLink(pChip->getOpticalGroupId());
    uint8_t cFunctionId = (pChip->getFrontEndType() == FrontEndType::LpGBT) ? LpGBTSlowControlWorker::SINGLE_WRITE_IC : LpGBTSlowControlWorker::SINGLE_WRITE_FE;
    auto    cCommand    = flpGBTSlowControlWorkerInterface->EncodeCommand(cFunctionId, pChip, pItem, pVerify);
    flpGBTSlowControlWorkerInterface->WriteCommand(cCommand);
    int cWaitCounter = 1000;
    while(!flpGBTSlowControlWorkerInterface->IsDone(cFunctionId) && (cWaitCounter != 0))
    {
        cWaitCounter--;
        continue;
    }
    if(cWaitCounter == 0)
    {
        uint16_t    cState           = flpGBTSlowControlWorkerInterface->GetStateFSM(cFunctionId);
        uint8_t     cWorkerState     = (cState & 0xFF);
        uint8_t     cFunctionState   = (cState & (0xFF << 8)) >> 8;
        std::string cWorkerStateDesc = LpGBTSlowControlWorker::WORKER_FSM_STATE_MAP.at(cWorkerState);
        std::string cFunctionStateDesc =
            (pChip->getFrontEndType() == FrontEndType::LpGBT) ? LpGBTSlowControlWorker::IC_FSM_STATE_MAP.at(cFunctionState) : LpGBTSlowControlWorker::FE_FSM_STATE_MAP.at(cFunctionState);
        LOG(ERROR) << BOLDRED << "D19cOpticalInterface::SingleWrite : Tool stuck - Worker state = " << cWorkerStateDesc << " - Function State = " << cFunctionStateDesc << RESET;
        return false;
    }
    uint8_t cTryCntr = flpGBTSlowControlWorkerInterface->GetTryCntr(cFunctionId);
    if(cTryCntr > 0)
    {
        uint8_t cMaxRetry = (pChip->getFrontEndType() == FrontEndType::LpGBT) ? fConfiguration.fMaxRetryIC : fConfiguration.fMaxRetryFE;
        LOG(ERROR) << BOLDRED << "D19cOpticalInterface::SingleWrite : Tried " << +cTryCntr << "/" << +cMaxRetry << " before success" << RESET;
    }
    auto    cReply     = flpGBTSlowControlWorkerInterface->ReadReply(1);
    uint8_t cErrorCode = (cReply[0] & (0xFF << 8)) >> 8;
    if(cErrorCode != 0)
    {
        LOG(ERROR) << BOLDRED << "D19cOpticalInterface::SingleWrite : Error Code is " << +cErrorCode << RESET;
        LOG(ERROR) << BOLDRED << "Chip code : " << +pChip->getChipCode() << " , register address 0x" << std::hex << +pItem.fAddress << RESET;
        return false;
    }
    uint8_t cReadBack = (cReply[0] & (0xFF << 0)) >> 0;
    if(pVerify)
    {
        if(cReadBack != pItem.fValue)
        {
            LOG(ERROR) << BOLDRED << "D19cOpticalInterface::SingleWrite : Wrong value read back" << RESET;
            return false;
        }
        pItem.fValue = cReadBack;
    }
    return true;
}

bool D19cOpticalInterface::SingleWrite(Chip* pChip, ChipRegItem& pItem)
{
    pChip->UpdateModifiedRegMap(pItem);
    return WriteChipRegister(pChip, pItem, false);
}

// for now this is just looping over single read
bool D19cOpticalInterface::MultiRead(Chip* pChip, std::vector<ChipRegItem>& pRegisterItems)
{
    bool cSuccess = true;
    for(auto& cRegItem: pRegisterItems) { cSuccess = cSuccess && SingleRead(pChip, cRegItem); }
    return cSuccess;
}

// for now this is just looping over single write
bool D19cOpticalInterface::MultiWrite(Chip* pChip, std::vector<ChipRegItem>& pRegisterItems)
{
    bool cSuccess = true;
    for(auto cRegItem: pRegisterItems) { cSuccess = cSuccess && SingleWrite(pChip, cRegItem); }
    return cSuccess;
}

// for now this is just a write followed by a read
bool D19cOpticalInterface::SingleWriteRead(Chip* pChip, ChipRegItem& pItem)
{
    pChip->UpdateModifiedRegMap(pItem);
    return WriteChipRegister(pChip, pItem, true);
}

// for now this is just a loop of WriteRead
bool D19cOpticalInterface::MultiWriteRead(Chip* pChip, std::vector<ChipRegItem>& pWriteRegs)
{
    bool cSuccess = true;
    for(auto cWriteReg: pWriteRegs) cSuccess = cSuccess && SingleWriteRead(pChip, cWriteReg);
    return cSuccess;
}

bool D19cOpticalInterface::MultiByteWriteI2C(Ph2_HwDescription::Chip* pChip, uint8_t pMasterId, uint8_t pMasterConfig, uint8_t pSlaveAddress, uint32_t pSlaveData)
{
    std::lock_guard<std::recursive_mutex> theGuard(fMutex);
    uint8_t                               cLinkId = pChip->getOpticalGroupId();
    flpGBTSlowControlWorkerInterface->SelectLink(cLinkId);
    uint8_t               cWorkerId   = LpGBTSlowControlWorker::BASE_ID + cLinkId;
    uint8_t               cFunctionId = LpGBTSlowControlWorker::MULTI_BYTE_WRITE_I2C;
    std::vector<uint32_t> cCommandVector;
    cCommandVector.clear();
    cCommandVector.push_back(cWorkerId << 24 | cFunctionId << 16 | pMasterId << 14 | pMasterConfig << 6);
    cCommandVector.push_back(pSlaveData << 8 | pSlaveAddress << 0);
    flpGBTSlowControlWorkerInterface->WriteCommand(cCommandVector);
    int cWaitCounter = 1000;
    while(!flpGBTSlowControlWorkerInterface->IsDone(cFunctionId) && (cWaitCounter != 0))
    {
        cWaitCounter--;
        continue;
    }
    if(cWaitCounter == 0)
    {
        uint16_t    cState             = flpGBTSlowControlWorkerInterface->GetStateFSM(cFunctionId);
        uint8_t     cWorkerState       = (cState & 0xFF);
        uint8_t     cFunctionState     = (cState & (0xFF << 8)) >> 8;
        std::string cWorkerStateDesc   = LpGBTSlowControlWorker::WORKER_FSM_STATE_MAP.at(cWorkerState);
        std::string cFunctionStateDesc = LpGBTSlowControlWorker::I2C_FSM_STATE_MAP.at(cFunctionState);
        LOG(ERROR) << BOLDRED << "D19cOpticalInterface::SingleByteReadI2C : Tool stuck - Worker state = " << cWorkerStateDesc << " - Function State = " << cFunctionStateDesc << RESET;
        return false;
    }
    uint8_t cTryCntr = flpGBTSlowControlWorkerInterface->GetTryCntr(cFunctionId);
    if(cTryCntr > 0) { LOG(ERROR) << BOLDRED << "D19cOpticalInterface::MultiByteWriteI2C : Tried " << +cTryCntr << "/" << +fConfiguration.fMaxRetryI2C << " before success" << RESET; }
    auto    cReply     = flpGBTSlowControlWorkerInterface->ReadReply(1);
    uint8_t cErrorCode = (cReply[0] & (0xFF << 8)) >> 8;
    if(cErrorCode != 0)
    {
        LOG(ERROR) << BOLDRED << "D19cOpticalInterface::MultiByteWriteI2C : Error Code is " << +cErrorCode << RESET;
        return false;
    }
    uint8_t cStatus = (cReply[0] & (0xFF << 0)) >> 0;
    if(cStatus != 4)
    {
        LOG(ERROR) << BOLDRED << "D19cOpticalInterface::MultiByteWriteI2C : I2C Status is " << +cStatus << RESET;
        return false;
    }
    return true;
}

uint8_t D19cOpticalInterface::SingleByteReadI2C(Ph2_HwDescription::Chip* pChip, uint8_t pMasterId, uint8_t pMasterConfig, uint8_t pSlaveAddress)
{
    std::lock_guard<std::recursive_mutex> theGuard(fMutex);
    uint8_t                               cLinkId = pChip->getOpticalGroupId();
    flpGBTSlowControlWorkerInterface->SelectLink(cLinkId);
    uint8_t               cWorkerId   = LpGBTSlowControlWorker::BASE_ID + cLinkId;
    uint8_t               cFunctionId = LpGBTSlowControlWorker::SINGLE_BYTE_READ_I2C;
    std::vector<uint32_t> cCommandVector;
    cCommandVector.clear();
    cCommandVector.push_back(cWorkerId << 24 | cFunctionId << 16 | pMasterId << 14 | pMasterConfig << 6);
    cCommandVector.push_back(pSlaveAddress << 0);
    flpGBTSlowControlWorkerInterface->WriteCommand(cCommandVector);
    int cWaitCounter = 1000;
    while(!flpGBTSlowControlWorkerInterface->IsDone(cFunctionId) && (cWaitCounter != 0))
    {
        cWaitCounter--;
        continue;
    }
    if(cWaitCounter == 0)
    {
        uint16_t    cState             = flpGBTSlowControlWorkerInterface->GetStateFSM(cFunctionId);
        uint8_t     cWorkerState       = (cState & 0xFF);
        uint8_t     cFunctionState     = (cState & (0xFF << 8)) >> 8;
        std::string cWorkerStateDesc   = LpGBTSlowControlWorker::WORKER_FSM_STATE_MAP.at(cWorkerState);
        std::string cFunctionStateDesc = LpGBTSlowControlWorker::I2C_FSM_STATE_MAP.at(cFunctionState);
        LOG(ERROR) << BOLDRED << "D19cOpticalInterface::SingleByteReadI2C : Tool stuck - Worker state = " << cWorkerStateDesc << " - Function State = " << cFunctionStateDesc << RESET;
        return 0;
    }
    uint8_t cTryCntr = flpGBTSlowControlWorkerInterface->GetTryCntr(cFunctionId);
    if(cTryCntr > 0) { LOG(ERROR) << BOLDRED << "D19cOpticalInterface::SingleByteReadI2C : Tried " << +cTryCntr << "/" << +fConfiguration.fMaxRetryI2C << " before success" << RESET; }
    auto    cReply     = flpGBTSlowControlWorkerInterface->ReadReply(1);
    uint8_t cErrorCode = (cReply[0] & (0xFF << 8)) >> 8;
    if(cErrorCode != 0)
    {
        LOG(ERROR) << BOLDRED << "D19cOpticalInterface::SingleByteReadI2C : Error Code is " << +cErrorCode << RESET;
        return 0;
    }
    uint8_t cReadBack = (cReply[0] & (0xFF << 0)) >> 0;
    return cReadBack;
}
} // namespace Ph2_HwInterface
